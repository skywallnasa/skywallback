var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var point = {
	x: Number,
	y: Number
}

var MessageSchema = new Schema({
	//user: {type : Schema.ObjectId, ref : 'User'},
	width: Number,
	height: Number,
	points: [point],
	lat: Number,
	lng: Number,
	yaw: Number,
	pitch: Number,
	roll: Number,
	x: Number,
	y: Number,
	z: Number
});

MessageSchema.methods = {
  calculate: function (cb) {
  	this.x = this.lat + 0.1*Math.cos(this.pitch/57.3)*Math.sin(this.yaw/57.3);
  	this.y = this.lng + 0.1*Math.cos(this.pitch/57.3)*Math.cos(this.yaw/57.3);
  	this.z =  0.1*Math.sin(this.pitch/57.3);
  	this.save(cb);
  }
}

mongoose.model('Message', MessageSchema);