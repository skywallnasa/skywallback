var mongoose = require('mongoose');
var http = require('http');
var Schema = mongoose.Schema;

var ImageInfoSchema = new Schema({
	image: String,
  address: String,
  lat: Number,
  lng: Number,
  yaw: Number,
  pitch: Number,
  roll: Number,
  x: Number,
  y: Number,
  z: Number
});

ImageInfoSchema.methods = {
  getAddress: function (cb) {
    var self = this;
    if (this.lat == 0 || this.lng == 0) {
      self.address = "";
      self.save(cb);
      return;
    }
    
    var options = {
      host: 'maps.googleapis.com',
      path: '/maps/api/geocode/json?latlng=' + this.lat + ',' + this.lng + '&sensor=true'
    };

    http.request(options, function(res) {
      var str = "";

      res.on('data', function (chunk) {
        str += chunk;
      });

      res.on('end', function () {
        self.address = JSON.parse(str).results[0].formatted_address;
        self.save(cb);
      });
    }).end();
    
  },
  calculate: function (cb) {
    this.x = this.lat + 0.1*Math.cos(this.pitch/57.3)*Math.sin(this.yaw/57.3);
    this.y = this.lng + 0.1*Math.cos(this.pitch/57.3)*Math.cos(this.yaw/57.3);
    this.z =  0.1*Math.sin(this.pitch/57.3);
    this.save(cb);
  }
}

mongoose.model('ImageInfo', ImageInfoSchema);