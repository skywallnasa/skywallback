// RESIZE HEADER

$(document).ready(function() {
	function resize(){
	    var heights = window.outerHeight;
	    $("#index #header").css({'height':heights + 'px'});
	}
	resize();
	window.onresize = function() {
	    resize();
	};
});

var filesList;
var offset = 0;

$.ajax({
  url : "/photos",
  type: "GET",
  success: function(data)
  {
    filesList = data;
    disabledPrevious();
    disabledNext();
    showImages();
  }
});

function showImages() {
	$("#feed").html('');
	var i;
	for (i = offset; i < 16 + offset; i++) {
    if (i < filesList.length) {
      var date = new Date(parseInt(filesList[i].image.split('.')[0]));
      $("#feed").append('<div class="col-md-3 col-sm-6 center">' + 
        '<img id="image0" class="img-responsive the_image"' +
        'src="photos/' + filesList[i].image + '">' +
        '<div class="the_info"><time>' + date + '</time><br><span>' + filesList[i].address + '</span></div></div>');
		}
	}
}

function previous() {
	offset -= 16;
	disabledPrevious();
  disabledNext();
	showImages();
}

function next() {
	offset += 16;
	disabledPrevious();
  disabledNext();
	showImages();
}

function disabledPrevious() {
	if (offset > 0) {
		$("#btnPrevious").prop('disabled', false);
	}
	else {
		$("#btnPrevious").prop('disabled', true);
	}
}

function disabledNext() {
	if (filesList.length - offset > 16) {
		$("#btnNext").prop('disabled', false);
	}
	else {
		$("#btnNext").prop('disabled', true);
	}
}