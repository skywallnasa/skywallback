var express = require('express');
var mongoose = require("mongoose");
var bodyParser = require('body-parser')
var multer  = require('multer')
var fs = require('fs');

require('./models/message.js');
require('./models/imageInfo.js');
var Message = mongoose.model('Message');
var ImageInfo = mongoose.model('ImageInfo');
var ObjectId = require('mongoose').Types.ObjectId; 

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public/'));

var PHOTOS_DIR = __dirname + '/public/photos/';

var db = mongoose.connection;
db.on('error', console.error);
db.once('open', function() {

	app.get('/gallery', function (req, res) {
	  fs.readFile('./public/gallery.html', function(error, content) {
      if (error) {
        res.writeHead(500);
        res.end();
      }
      else {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(content, 'utf-8');
      }
    });
	});

  app.post('/message', function (req, res) {
    var message = new Message({
      width: req.body.width,
      height: req.body.height,
      points: req.body.points,
      lat: req.body.lat,
      lng: req.body.lng,
      yaw: req.body.yaw,
      pitch: req.body.pitch,
      roll: req.body.roll
    });
    message.calculate(function() {
      res.send(message);
    });
  });

	app.post('/messages', function (req, res) {
    var lat = req.body.lat
    var lng = req.body.lng
		Message.find({ x: { $lt: 1 + lat, $gt: lat -1}, y: { $lt: 1 + lng, $gt: lng -1} }, function(err, obj) {
      res.send(obj);
		});
	});

	app.post('/upload', function (req, res) {
		imageId = Date.now() + '.jpg';
		var imageInfo = new ImageInfo({
			image: imageId,
			lat: req.headers.lat,
			lng: req.headers.lng,
			yaw: req.headers.yaw,
			pitch: req.headers.pitch,
			roll: req.headers.roll
		});
    imageInfo.getAddress();
		imageInfo.calculate();

		var data = new Buffer('');
		req.on('data', function(chunk) {
			data = Buffer.concat([data, chunk]);
		});
		req.on('end', function(){
	    fs.writeFile(PHOTOS_DIR + imageId, data, function(err){
	        if (err) throw err
	        else console.log('Photo write ' + PHOTOS_DIR + imageId);
	    });
	    res.end();
	  });
	});

	app.get('/photos', function (req, res) {
    ImageInfo.find(function(err, obj) {
      res.send(obj);
    })
	});

	app.get('/photos/:file', function (req, res){
	    file = req.params.file;
	    var img = fs.readFileSync(PHOTOS_DIR + file);
	    res.writeHead(200, {'Content-Type': 'image/jpg' });
	    res.end(img, 'binary');
	});

	var port = process.env.NODE_PORT || 8080;
	var server = app.listen(port, function () {
	  var host = server.address().address;
	  console.log('App listening at http://%s:%s', host, port);
	});
});

mongoose.connect('mongodb://localhost/skywall');
